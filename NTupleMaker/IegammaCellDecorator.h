


#ifndef NTUPLEMAKER_IEGAMMACELLDECORATOR_H
#define NTUPLEMAKER_IEGAMMACELLDECORATOR_H 1


#include "AsgTools/AsgTool.h"


using namespace std;

class IegammaCellDecorator : virtual public asg::IAsgTool { 

	ASG_TOOL_INTERFACE(IegammaCellDecorator)
public: 
	
	virtual StatusCode initialize()  = 0;
	virtual StatusCode decorate(const xAOD::Egamma* egamma) = 0;
	virtual StatusCode finalize() = 0;


};

#endif //> !NTUPLEMAKER_IEGAMMACELLDECORATOR_H
