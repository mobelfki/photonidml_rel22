


#ifndef NTUPLEMAKER_IelectronSELECTOR_H
#define NTUPLEMAKER_IelectronSELECTOR_H 1


#include "AsgTools/AsgTool.h"
#include <xAODEventInfo/EventInfo.h>

using namespace std;

class IelectronSelector : virtual public asg::IAsgTool { 

	ASG_TOOL_INTERFACE(IelectronSelector)
public: 
	
	
	
	virtual StatusCode initialize()  = 0;
	virtual StatusCode execute(const xAOD::EventInfo *eventInfo) = 0;
	virtual StatusCode finalize() = 0;


};

#endif //> !NTUPLEMAKER_IPHOTONSELECTOR_H
