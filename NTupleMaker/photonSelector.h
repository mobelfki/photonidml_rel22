


#ifndef NTUPLEMAKER_PHOTONSELECTOR_H
#define NTUPLEMAKER_PHOTONSELECTOR_H 1

#include <AsgTools/AnaToolHandle.h>
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"


#include <EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h>
#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include "ElectronPhotonSelectorTools/EGammaAmbiguityTool.h"
#include "EGammaVariableCorrection/ElectronPhotonVariableCorrectionTool.h"

#include "xAODEgamma/PhotonContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"

#include "TruthUtils/HepMCHelpers.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODBase/IParticle.h"


#include "NTupleMaker/IphotonSelector.h"


using namespace std;

class photonSelector : public :: asg::AsgTool, virtual public IphotonSelector { 

	ASG_TOOL_CLASS(photonSelector, IphotonSelector)
public: 
	
	
	photonSelector (const std::string& name = "photonSelector");
	virtual StatusCode initialize() override;
	virtual StatusCode execute(const xAOD::EventInfo *eventInfo) final;
	virtual StatusCode finalize() final;
	
	
private: 
	
	ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_photonCalibrationAndSmearingTool {this, "EgammaCorrectionTool", "", "energy correction tool"};
    	ToolHandle<CP::IIsolationCorrectionTool> m_photonIsolationCorrectionTool {this, "IsolationCorrectionTool", "", "photon isolation calibration"};
    	ToolHandle<IEGammaAmbiguityTool>  m_photonAmbiguityTool {this, "IEGammaAmbiguityTool", "", "photon ambiguity tool"};
    	ToolHandle<ElectronPhotonVariableCorrectionTool> m_fudgeMCTool {this, "ElectronPhotonVariableCorrectionTool", "", "shower shape correction tool"}; 
    	
    	
	AsgPhotonIsEMSelector* m_photonSelectionTool_loose;
	AsgPhotonIsEMSelector* m_photonSelectionTool_tightinc;
	AsgPhotonIsEMSelector* m_photonSelectionTool_tightdep;
	
	AsgPhotonIsEMSelector* m_photonSelectionTool_tightisEM;	
	AsgPhotonIsEMSelector* m_photonSelectionTool_looseisEM;
	
	CP::IsolationSelectionTool* m_photonIsolationTool_loose;
	CP::IsolationSelectionTool* m_photonIsolationTool_tight;			
	
	std::string m_inputContainer;
	std::string m_outputContainer;
	
	double m_minPt;
	double m_minEta;
	
	
	std::string m_cfg_offline_loose;
	std::string m_cfg_offline_tightinc;
	std::string m_cfg_offline_tightdep;		

	std::string m_cfg_online_loose;
	std::string m_cfg_online_tight;
	
	std::string m_isolooseWP;
	std::string m_isotightWP;
	
	bool m_isMC;


private: 
	virtual void decorateWithTruthInfo( xAOD::Photon* egamma, bool isMC);
	virtual bool notFromHadron(const xAOD::TruthParticle* ptcl);
	
public:
	virtual void decorateWithInfo(xAOD::Photon* egamma);	
	
};

#endif //> !NTUPLEMAKER_PHOTONSELECTOR_H
