


#ifndef NTUPLEMAKER_IPHOTONSELECTOR_H
#define NTUPLEMAKER_IPHOTONSELECTOR_H 1


#include "AsgTools/AsgTool.h"
#include "xAODTruth/TruthParticle.h"

using namespace std;

class IphotonSelector : virtual public asg::IAsgTool { 

	ASG_TOOL_INTERFACE(IphotonSelector)
public: 
	
	
	
	virtual StatusCode initialize()  = 0;
	virtual StatusCode execute(const xAOD::EventInfo *eventInfo) = 0;
	virtual StatusCode finalize() = 0;
	
private: 
	virtual void decorateWithTruthInfo(xAOD::Photon* egamma, bool isMC) = 0;
	virtual bool notFromHadron(const xAOD::TruthParticle* ptcl) = 0;
	
public:
	
	virtual void decorateWithInfo(xAOD::Photon* egamma) = 0;	


};

#endif //> !NTUPLEMAKER_IPHOTONSELECTOR_H
