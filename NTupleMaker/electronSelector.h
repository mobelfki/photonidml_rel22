


#ifndef NTUPLEMAKER_ELECTRONSELECTOR_H
#define NTUPLEMAKER_ELECTRONSELECTOR_H 1

#include <AsgTools/AnaToolHandle.h>
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"

#include "NTupleMaker/IelectronSelector.h"

#include <EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h>
#include <EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h>
#include <IsolationSelection/IIsolationSelectionTool.h>
#include "IsolationCorrections/IIsolationCorrectionTool.h"

#include <xAODEgamma/ElectronContainer.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include <xAODEventInfo/EventInfo.h>
#include "xAODCore/ShallowCopy.h"

using namespace std;

class electronSelector : public :: asg::AsgTool, virtual public IelectronSelector { 

	ASG_TOOL_CLASS(electronSelector, IelectronSelector)
public: 
	
	
	electronSelector (const std::string& name = "electronSelector");
	virtual StatusCode initialize() override;
	virtual StatusCode execute(const xAOD::EventInfo *eventInfo) final;
	virtual StatusCode finalize() final;
	
	
private: 

	ToolHandle<IAsgElectronLikelihoodTool> m_electronSelectionTool {this, "AsgElectronLikelihoodTool", "", "electron ID tool"};
    	ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_electronCalibrationAndSmearingTool{this, "EgammaCorrectionTool", "", "energy correction tool"};
    	ToolHandle<CP::IIsolationSelectionTool> m_electronIsolationTool {this, "IsolationSelectionTool", "", "electron Iso tool"};
    	ToolHandle<CP::IIsolationCorrectionTool> m_electronIsolationCorrectionTool {this, "IsolationCorrectionTool", "", "electron isolation calibration"};
    	

	std::string m_inputContainer;
	std::string m_outputContainer;
	std::string m_vertexContainer;
	
	double m_minPt;
	double m_minEta;
	double m_minZ0;
	double m_minD0;

};

#endif //> !NTUPLEMAKER_electronSelector_H
