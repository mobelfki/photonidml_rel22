


#ifndef NTUPLEMAKER_muonSelector_H
#define NTUPLEMAKER_muonSelector_H 1

#include <AsgTools/AnaToolHandle.h>
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"

#include "NTupleMaker/ImuonSelector.h"

#include "MuonMomentumCorrections/MuonCalibTool.h"
#include <IsolationSelection/IIsolationSelectionTool.h>
#include "MuonSelectorTools/MuonSelectionTool.h"

#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include <xAODEventInfo/EventInfo.h>
#include "xAODCore/ShallowCopy.h"
#include "xAODEventInfo/EventInfo.h"

using namespace std;

class muonSelector : public :: asg::AsgTool, virtual public ImuonSelector { 

	ASG_TOOL_CLASS(muonSelector, ImuonSelector)
public: 
	
	
	muonSelector (const std::string& name = "muonSelector");
	virtual StatusCode initialize() override;
	virtual StatusCode execute() final;
	virtual StatusCode finalize() final;
	
	
private: 


	ToolHandle<CP::MuonCalibTool>          m_muonCalibrationTool {this, "MuonCalibTool", "", "muon calibraiton tool"}; 
	ToolHandle<CP::MuonSelectionTool>      m_muonSelectionTool   {this, "MuonSelectionTool", "", "muon selection tool"};
	ToolHandle<CP::IIsolationSelectionTool> m_muonIsolationTool   {this, "IsolationSelectionTool", "", "electron Iso tool"};
	
	std::string m_inputContainer;
	std::string m_outputContainer;	
	std::string m_vertexContainer;
	
	double m_minPt;
	double m_minEta;
	double m_minZ0;
	double m_minD0;


};

#endif //> !NTUPLEMAKER_muonSelector_H
