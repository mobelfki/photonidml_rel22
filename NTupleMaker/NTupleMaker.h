//
// Header files
//


#ifndef NTUPLEMAKER_NTUPLEMAKE_H
#define NTUPLEMAKER_NTUPLEMAKE_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "GoodRunsLists/IGoodRunsListSelectorTool.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include "xAODEventInfo/EventInfo.h"

#include <NTupleMaker/IelectronSelector.h>
#include <NTupleMaker/ImuonSelector.h>
#include <NTupleMaker/IphotonSelector.h>
#include <NTupleMaker/IegammaCellDecorator.h>


#include "xAODCore/ShallowCopy.h"
#include "TTree.h"


using namespace std;

class NTupleMaker : public :: AthAnalysisAlgorithm { 

public: 

	NTupleMaker( const std::string& name, ISvcLocator* pSvcLocator );
	
	virtual ~NTupleMaker();
	virtual StatusCode initialize();
	virtual StatusCode execute();
	virtual StatusCode finalize();
	
private: 
       
       StatusCode singlePhoton();
           
private: 
	
	TTree* m_tree = 0;

	std::vector<std::string> m_Triggers;
	ToolHandle< IGoodRunsListSelectionTool > m_GRLTool;
	ToolHandle< CP::IPileupReweightingTool > m_PUTool;
	ToolHandle<Trig::TrigDecisionTool> m_trgD_tool;
	ToolHandle<Trig::IMatchingTool> m_trgM_tool;
	
	ToolHandle<IelectronSelector> m_electronSelector {this, "electronSelector", "", "tool to select electrons"};
	ToolHandle<ImuonSelector> m_muonSelector {this, "muonSelector", "", "tool to select muons"};
	ToolHandle<IphotonSelector> m_photonSelector {this, "photonSelector", "",  "tool to select photons"};
	ToolHandle<IegammaCellDecorator> m_egammaCellDecorator {this, "egammaCellDecorator", "", "tool to decorate egamma obejct with cells"};

	std::vector<std::string> m_grlVec;
 	std::vector<std::string> m_brlVec;
 	std::vector<std::string> m_prw;
  	std::string m_GRLname = "grl_tool";
  	bool m_isMC;
	std::string m_Algo;
	bool m_saveCells;
	std::vector<int> m_N_eta;
	std::vector<int> m_N_phi;
	std::vector<int> m_N_lr;
	std::vector<std::string> m_object;
	
	
	//tree variables
	
	int* TrigDecisions;
	float    m_Avgmu;
	float    m_mcwgt;
	float    m_Avgmuwgt; 
	long int m_runnumber;
	long int m_mcchannel;
	long int m_evtnumber;
	
	float photon_pt;
	float photon_eta;
	float photon_phi; 
	float photon_e;
	
	float photon_cluster_pt;
	float photon_cluster_e;
	float photon_cluster_phi;
	float photon_cluster_eta;
	
	float photon_cluster_eta1;
	float photon_cluster_eta2;
	
	int   photon_convFlag;
	float photon_convR;
	
		 
	bool photon_looseOff;
	bool photon_looseOn; 
	bool photon_tightOffInc;
	bool photon_tightOffDep;
	bool photon_tightOn;
	
	bool photon_isoloose;
	bool photon_isotight;
		
	bool photon_istruthmatch;
	bool photon_istruthconversion;
	int  photon_pdg;
	int  photon_truth_origin;
	int  photon_truth_type;
	int  photon_mother_pdg;
	
	std::map<TString, vector<double>> m_cells_e;
	std::map<TString, vector<double>> m_cells_eta;
	std::map<TString, vector<double>> m_cells_phi;
	std::map<TString, vector<bool>> m_cells_incluster;
	std::map<TString, vector<int>> m_cells_index;
	std::map<TString, int> m_cluster_size; 
	
	std::map<TString, float> m_showershapes;
	std::map<TString, float> m_isolationvar;
	
	vector<std::string> m_showers    = {"rhad1", "rhad", "e277", "reta", "rphi", "weta2", "f1", "wtots1", "weta1", "fracs1", "eratio", "deltae"};
	vector<std::string> m_showerfalg = {"", "unfudged_"};
	vector<std::string> m_isovars    = {"ptcone20", "ptcone30", "ptcone40", "etcone20", "etcone30", "etcone40", "topoetcone20", "topoetcone30", "topoetcone40", "ptvarcone20", "ptvarcone30", "ptvarcone40"}; 
	
private: 

	void fillPhotonVariables(const xAOD::Photon* egamma);

	void fillClusterCells(const xAOD::Egamma* egamma, TString object);
	
	void fillShowerShapes(const xAOD::Egamma* egamma, TString object);
	
	void fillIsolationVars(const xAOD::Egamma* egamma, TString object);

	inline void connectTree(TTree*& tree) {
	
		tree->Branch("event.avgmu", &m_Avgmu);
		tree->Branch("event.mcwgt", &m_mcwgt);
		tree->Branch("event.muwgt", &m_Avgmuwgt);
		tree->Branch("event.runnumber", &m_runnumber);
		tree->Branch("event.mcchannel", &m_mcchannel);
		tree->Branch("event.mcnumber", &m_evtnumber);
		
		tree->Branch("photon.pt", &photon_pt);
		tree->Branch("photon.eta", &photon_eta);
		tree->Branch("photon.phi", &photon_phi);
		tree->Branch("photon.e", &photon_e);
		
		tree->Branch("photon_cluster.pt",  &photon_cluster_pt);
		tree->Branch("photon_cluster.eta", &photon_cluster_eta);
		tree->Branch("photon_cluster.phi", &photon_cluster_phi);
		tree->Branch("photon_cluster.e",   &photon_cluster_e);
		
		tree->Branch("photon_cluster.eta1", &photon_cluster_eta1);
		tree->Branch("photon_cluster.eta2", &photon_cluster_eta2);	
		
		tree->Branch("photon.convFlag", &photon_convFlag);
		tree->Branch("photon.convR", &photon_convR);									
		
		tree->Branch("photon.looseOff", &photon_looseOff);
		tree->Branch("photon.looseOn",  &photon_looseOn);
		tree->Branch("photon.tightOffInc", &photon_tightOffInc);
		tree->Branch("photon.tightOffDep", &photon_tightOffDep);
		tree->Branch("photon.tightOn",  &photon_tightOn);
		tree->Branch("photon.isoloose", &photon_isoloose);
		tree->Branch("photon.isotight", &photon_isotight);
		
		tree->Branch("photon.istruthmatch",        &photon_istruthmatch);
		tree->Branch("photon.istruthconversion",   &photon_istruthconversion);
		tree->Branch("photon.pdg",                 &photon_pdg);
		tree->Branch("photon.truthOrigin",        &photon_truth_origin);
		tree->Branch("photon.truthType",          &photon_truth_type);
		tree->Branch("photon.truthMotherPDG", &photon_mother_pdg);
	}
	
	inline void connectCells(TTree*& tree) {
	
		for(auto obj : m_object) {
		for(auto lr  : m_N_lr)  {
		
			for(unsigned int i = 0; i<m_N_eta.size(); i++){
			
				int eta = m_N_eta[i];
				int phi = m_N_phi[i];
				
				if(lr != 4) {
		
				m_cells_e[Form("%s.%ix%iClusterLr%iE", obj.data(), eta, phi, lr)].clear();
				m_cells_eta[Form("%s.%ix%iClusterLr%iEta", obj.data(), eta, phi, lr)].clear();
				m_cells_phi[Form("%s.%ix%iClusterLr%iPhi", obj.data(), eta, phi, lr)].clear();
				m_cells_incluster[Form("%s.%ix%iClusterLr%iInCluster", obj.data(), eta, phi, lr)].clear();
				m_cluster_size[Form("%s.%ix%iClusterLr%iSize", obj.data(), eta, phi, lr)] = 0;
				
				}else{
				
				m_cells_e[Form("%s.%ix%iClusterLr%iE", obj.data(), eta, phi, lr)].clear();
				m_cells_eta[Form("%s.%ix%iClusterLr%iEta", obj.data(), eta, phi, lr)].clear();
				m_cells_phi[Form("%s.%ix%iClusterLr%iPhi", obj.data(), eta, phi, lr)].clear();
				m_cells_index[Form("%s.%ix%iClusterLr%iIndex", obj.data(), eta, phi, lr)].clear();
				
				}
				
				if(lr != 4) {
				
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iE", eta, phi, lr)), &m_cells_e[Form("%s.%ix%iClusterLr%iE", obj.data(), eta, phi, lr)]);
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iEta", eta, phi, lr)), &m_cells_eta[Form("%s.%ix%iClusterLr%iEta", obj.data(), eta, phi, lr)]);
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iPhi", eta, phi, lr)), &m_cells_phi[Form("%s.%ix%iClusterLr%iPhi", obj.data(), eta, phi, lr)]);
				
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iInCluster", eta, phi, lr)), &m_cells_incluster[Form("%s.%ix%iClusterLr%iInCluster", obj.data(), eta, phi, lr)]);
				
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iSize", eta, phi, lr)), &m_cluster_size[Form("%s.%ix%iClusterLr%iSize", obj.data(), eta, phi, lr)]);
				
				}else{
				
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iE", eta, phi, lr)), &m_cells_e[Form("%s.%ix%iClusterLr%iE", obj.data(), eta, phi, lr)]);
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iEta", eta, phi, lr)), &m_cells_eta[Form("%s.%ix%iClusterLr%iEta", obj.data(), eta, phi, lr)]);
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iPhi", eta, phi, lr)), &m_cells_phi[Form("%s.%ix%iClusterLr%iPhi", obj.data(), eta, phi, lr)]);
				
				tree->Branch(Form("%s.%s", obj.data(), Form("%ix%iClusterLr%iIndex", eta, phi, lr)), &m_cells_index[Form("%s.%ix%iClusterLr%iIndex", obj.data(), eta, phi, lr)]);
				
				}
				
			}	
		}}
	
	}
	
	inline void connectShowerShapes(TTree*& tree) {
	
		for(auto obj : m_object) {
		for(auto sw  : m_showers) {
		for(auto flag: m_showerfalg) {		
		
			m_showershapes[Form("%s.%s%s", obj.data(), flag.data(), sw.data())] = 0.0;
			tree->Branch(Form("%s.%s%s", obj.data(), flag.data(), sw.data()), &m_showershapes[Form("%s.%s%s", obj.data(), flag.data(), sw.data())]);
		
		}
		}
		}
	}
	
	inline void connectIsolationVars(TTree*& tree) {
	
		for(auto obj : m_object) {
		for(auto iso : m_isovars) {
		
			 m_isolationvar[Form("%s.iso_%s", obj.data(), iso.data()) ] = 0.0;
			 tree->Branch(Form("%s.iso_%s", obj.data(), iso.data()), &m_isolationvar[Form("%s.iso_%s", obj.data(), iso.data()) ]);	
		}}
	
	}	

};

#endif //> !NTUPLEMAKER_NTUPLEMAKER_H
