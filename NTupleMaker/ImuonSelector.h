


#ifndef NTUPLEMAKER_ImuonSelector_H
#define NTUPLEMAKER_ImuonSelector_H 1


#include "AsgTools/AsgTool.h"


using namespace std;

class ImuonSelector : virtual public asg::IAsgTool { 

	ASG_TOOL_INTERFACE(ImuonSelectorr)
public: 
	
	
	
	virtual StatusCode initialize()  = 0;
	virtual StatusCode execute() = 0;
	virtual StatusCode finalize() = 0;


};

#endif //> !NTUPLEMAKER_ImuonSelector_H
