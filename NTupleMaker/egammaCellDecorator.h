

#ifndef NTUPLEMAKER_EGAMMACELLDECORATOR_H
#define NTUPLEMAKER_EGAMMACELLDECORATOR_H 1

#include <AsgTools/AnaToolHandle.h>
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"

#include "xAODEgamma/EgammaContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCore/ShallowCopy.h"

#include "CaloEvent/CaloClusterCellLink.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloUtils/CaloClusterStoreHelper.h"
#include "CaloUtils/CaloCellDetPos.h"
#include "CaloClusterCorrection/CaloFillRectangularCluster.h"

#include "CaloDetDescr/CaloDetDescrManager.h"
#include "StoreGate/ReadCondHandleKey.h"

#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/ToolHandle.h"

#include "NTupleMaker/IegammaCellDecorator.h"
#include "TTree.h"

using namespace std;

class egammaCellDecorator : public :: asg::AsgTool, virtual public IegammaCellDecorator { 

	ASG_TOOL_CLASS(egammaCellDecorator, IegammaCellDecorator)
public: 
	
	
	egammaCellDecorator (const std::string& name = "egammaCellDecorator");
	virtual StatusCode initialize() override;
	virtual StatusCode decorate(const xAOD::Egamma* egamma) final;
	virtual StatusCode finalize() final;
	
	
private: 

	SG::ReadCondHandleKey<CaloDetDescrManager> m_caloMgrKey { this, "CaloDetDescrManager", "CaloDetDescrManager", "SG Key for CaloDetDescrManager in the Condition Store" };


	ToolHandle<CaloClusterProcessor> m_caloFillRectangularTool_7x11;
	ToolHandle<CaloClusterProcessor> m_caloFillRectangularTool_5x5;
	ToolHandle<CaloClusterProcessor> m_caloFillRectangularTool_3x5;		
	
	CaloFillRectangularCluster *m_CaloTool;
	
	std::map<TString, CaloFillRectangularCluster*> m_CaloTools;
	
	std::string  m_CaloCellContainer;
	vector<int>  m_etaSize;
	vector<int>  m_phiSize;
	
	//std::map<TString, vector<double>> m_cells_e;
	//std::map<TString, vector<double>> m_cells_eta;
	//std::map<TString, vector<double>> m_cells_phi;
	//std::map<TString, int> m_cluster_size; 
};

#endif //> !NTUPLEMAKER_EGAMMACELLDECORATOR_H
