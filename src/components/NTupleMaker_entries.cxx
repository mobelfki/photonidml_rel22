

#include <NTupleMaker/NTupleMaker.h>
#include <NTupleMaker/photonSelector.h>
#include <NTupleMaker/electronSelector.h>
#include <NTupleMaker/muonSelector.h>
#include <NTupleMaker/egammaCellDecorator.h>

DECLARE_COMPONENT( NTupleMaker )
DECLARE_COMPONENT( photonSelector )
DECLARE_COMPONENT( electronSelector )
DECLARE_COMPONENT( muonSelector )
DECLARE_COMPONENT( egammaCellDecorator )
