


#include <NTupleMaker/electronSelector.h>



electronSelector :: electronSelector (const std::string& name) : AsgTool (name) {


	declareProperty("electronSelectionTool", m_electronSelectionTool, "the AsgElectronLikelihood tool");
	declareProperty("electronCalibrationAndSmearingTool", m_electronCalibrationAndSmearingTool, "the EgammaCalibrationAndSmearing tool");
	declareProperty("electronIsolationTool", m_electronIsolationTool, "the IsolationSelection tool");
	declareProperty("electronIsolationCorrectionTool", m_electronIsolationCorrectionTool, "the IsolationCorrection tool");

	declareProperty("inputContainer",  m_inputContainer  = "Electrons");
	declareProperty("outputContainer", m_outputContainer = "selectedElectrons");
	declareProperty("vertexContainer", m_vertexContainer = "PrimaryVertices"); 
	
	declareProperty("minPt", m_minPt = 10.);
	declareProperty("minEta", m_minEta = 2.5);
	declareProperty("minZ0", m_minZ0 = 10.);
	declareProperty("mind0", m_minD0 = 10.);
}


StatusCode electronSelector::initialize() {

	ATH_MSG_INFO ("Initializing " << name() << "...");
	
	CHECK(m_electronSelectionTool.retrieve() ); //electron ID tool
  	CHECK(m_electronCalibrationAndSmearingTool.retrieve() );
  	CHECK(m_electronIsolationTool.retrieve() );
  	CHECK(m_electronIsolationCorrectionTool.retrieve() );


	return StatusCode::SUCCESS;
}

StatusCode electronSelector::finalize() {

       ATH_MSG_INFO ("Finalizing " << name() << "...");

       return StatusCode::SUCCESS;
    
}

StatusCode electronSelector::execute(const xAOD::EventInfo *eventInfo) {

	ATH_MSG_INFO ("Executing " << name() << "...");

	const xAOD::ElectronContainer* electrons = nullptr;
  	CHECK (evtStore()->retrieve (electrons, m_inputContainer));
  	
	const xAOD::VertexContainer *vtxContainer = nullptr;
	CHECK(evtStore()->retrieve(vtxContainer, m_vertexContainer));
	
	const xAOD::Vertex* pvtx = nullptr;
	if(vtxContainer->size() > 0) pvtx = (*vtxContainer)[0];
	
	double m_pvZ = pvtx->z();
	
	std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer*> electronsCopy = xAOD::shallowCopyContainer(*electrons);
	xAOD::ElectronContainer::iterator electron= electronsCopy.first->begin();
	xAOD::ElectronContainer::iterator elLast  = electronsCopy.first->end();

	
	int i = 0;
	for(; electron != elLast; ++electron)
	{
	
		m_electronCalibrationAndSmearingTool->setRandomSeed(eventInfo->eventNumber() + 100 * i++);
		if( m_electronCalibrationAndSmearingTool->applyCorrection(**electron) != CP::CorrectionCode::Ok) ATH_MSG_DEBUG("cannot calibrate the electron");
		
		if( m_electronIsolationCorrectionTool->applyCorrection(**electron) != CP::CorrectionCode::Ok) ATH_MSG_DEBUG("cannot correct the electron isolation");
		
		if(!m_electronSelectionTool->accept((*electron))) continue;
		if(!m_electronIsolationTool->accept(*(*electron))) continue;
	
		if( (*electron)->pt() * 1e-3 < m_minPt ) continue;
		if( fabs((*electron)->eta()) > m_minEta ) continue; 
		
		if( fabs((*electron)->eta()) > 1.37 && fabs((*electron)->eta()) < 1.52) continue; // remove crack region 
		
		const ElementLink< xAOD::TrackParticleContainer >& link = (*electron)->trackParticleLink();
		
		double d0   = 100.;
		double d0err = 100.; 
		double z0   = 100.;
		
		if( link.isValid() ) {
			const xAOD::TrackParticle* track = *link;
			d0    = fabs(track->d0());
			d0err = sqrt(track->definingParametersCovMatrix () (0,0));
			z0    = fabs(track->z0() + track->vz() - m_pvZ) * sin(track->theta());		
		}
		
		if( z0 > m_minZ0) continue; 
		if( d0/d0err > m_minD0 ) continue; 
	} 


	CHECK(evtStore()->record( electronsCopy.first,    m_outputContainer));
  	CHECK(evtStore()->record( electronsCopy.second,   m_outputContainer+"Aux"));


	return StatusCode::SUCCESS;
}
