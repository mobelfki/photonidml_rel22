//

//

#include "NTupleMaker/NTupleMaker.h"


NTupleMaker::NTupleMaker( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ), m_GRLTool("GoodRunsListSelectorTool")

{
	declareProperty("triggers", m_Triggers);
	declareProperty("GoodRunsListArray", m_grlVec);
	declareProperty("PRWFiles", m_prw);
	declareProperty("isMC", m_isMC = true);
	declareProperty("Algo", m_Algo = "SinglePhoton");
	declareProperty("saveCells", m_saveCells);
	declareProperty("N_eta", m_N_eta);
	declareProperty("N_phi", m_N_phi);
	declareProperty("N_lr",  m_N_lr);
	declareProperty("saveObjectCells", m_object);
}

NTupleMaker::~NTupleMaker() {

}

StatusCode NTupleMaker::initialize() {

    ATH_MSG_INFO ("Initializing " << name() << "...");
    ServiceHandle<ITHistSvc> histSvc("THistSvc", name());
    CHECK( histSvc.retrieve() );
    
    if(m_N_eta.size() != m_N_phi.size()) {
		ATH_MSG_ERROR("eta, phi clusters size are not correctly set");
    }
    
    // retrieve GRL tool 

    //m_GRLTool.setTypeAndName("NTupleMaker/GoodRunsListSelectionTool");    
    //CHECK(m_GRLTool.retrieve());
    //bool regOk = m_GRLTool->registerGRLSelector(m_GRLname, m_grlVec, m_brlVec);
    //if (!regOk) {
    //	ATH_MSG_ERROR ("initialize() :: Failure to register this GRL Selector with name <" << m_GRLname << ">. Return failure.");
    //	return StatusCode::FAILURE;
    //}
    
    // retrieve PU tool 
   
   m_PUTool.setTypeAndName("CP::PileupReweightingTool/prw");
   //asg::setProperty(m_PUTool, "ConfigFiles", m_prw)
   CHECK(m_PUTool.retrieve());
   
   //trigger tools 
   m_trgD_tool.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
   CHECK(m_trgD_tool.retrieve());
   
   m_trgM_tool.setTypeAndName("Trig::MatchingTool/TrigMatchingTool");
   CHECK(m_trgM_tool.retrieve());
   
   
   m_electronSelector.setTypeAndName("electronSelector/electronSelector");
   CHECK(m_electronSelector.retrieve());
   
   //m_muonSelector.setTypeAndName("muonSelector/muonSelector");
   //CHECK(m_muonSelector.retrieve());
   
   m_photonSelector.setTypeAndName("photonSelector/photonSelector");
   CHECK(m_photonSelector.retrieve());
   
   m_egammaCellDecorator.setTypeAndName("egammaCellDecorator/egammaCellDecorator");
   CHECK(m_egammaCellDecorator.retrieve());
   
   m_tree = new TTree("tree", m_Algo.c_str());
   CHECK( histSvc->regTree("/OUTPUT/", m_tree) );
   
   TrigDecisions = new int [m_Triggers.size()];
   
   for(unsigned int i = 0; i<m_Triggers.size(); i++) {
    	TrigDecisions[i] = 0;
    	m_tree->Branch(Form("Trig_%s", m_Triggers[i].data()), &(TrigDecisions[i]));
   }
   
   connectTree(m_tree);
   connectShowerShapes(m_tree);
   connectIsolationVars(m_tree);
   
   if(m_saveCells) connectCells(m_tree);
   
   return StatusCode::SUCCESS;
}

StatusCode NTupleMaker::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");

    return StatusCode::SUCCESS;
    
}

StatusCode NTupleMaker::execute() {

	ATH_MSG_DEBUG ("Executing " << name() << "...");
	
	const xAOD::EventInfo *eventInfo = nullptr;
	CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
	
	// check GRL 
	
	//if(!m_isMC and !m_GRLTool->passRunLB(*eventInfo)) return StatusCode::SUCCESS;
	
	// check LAr 
	
	if(!m_isMC and eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) return StatusCode::SUCCESS;
	
	// check Tile 
	
	if(!m_isMC and eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) return StatusCode::SUCCESS;
	
	// check Core 
	
	if(!m_isMC and eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) return StatusCode::SUCCESS;
	
	// check Background 
	
	if(!m_isMC and eventInfo->isEventFlagBitSet(xAOD::EventInfo::Background, 20)) return StatusCode::SUCCESS;
	
	// check SCT 
	
	if(!m_isMC and eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) return StatusCode::SUCCESS;
	
	
	
	
	int cnt = 0;
	for (auto thisTrig : m_Triggers) {
		if(m_trgD_tool->getChainGroup(thisTrig)->isPassed()) cnt++;
	}

//	if(cnt == 0) return StatusCode::SUCCESS;
	
	for( unsigned i = 0; i<m_Triggers.size(); i++) {
  	
  		int passTrigger = m_trgD_tool->isPassed(m_Triggers[i]);
  		TrigDecisions[i] = passTrigger;
  	}
	
	CHECK(m_PUTool->apply(*eventInfo));
	
	m_Avgmu    = m_PUTool->getCorrectedAverageInteractionsPerCrossing(*eventInfo, true);
	
	m_Avgmuwgt = m_isMC ? m_PUTool->getCombinedWeight(*eventInfo) : 1.0;
	
	m_mcwgt    = (m_isMC && eventInfo->mcEventWeights().size() > 0) ? eventInfo->mcEventWeights()[0] : 1.0 ;
	
	m_runnumber = eventInfo->runNumber();
	
	m_evtnumber = eventInfo->eventNumber();
	
	m_mcchannel = eventInfo->mcChannelNumber();
	
	/*
   	if(TString(m_Algo).Contains("RadiativeZ")){ 
   		CHECK(m_electronSelector->execute(eventInfo));
   		CHECK(m_muonSelector->execute());
   	}
   	*/
   	
   	CHECK(m_photonSelector->execute(eventInfo));
   	
   	CHECK(singlePhoton());
   	
	return StatusCode::SUCCESS;
}

StatusCode NTupleMaker::singlePhoton(){

	const xAOD::PhotonContainer* photons = nullptr;
	CHECK (evtStore()->retrieve (photons, "selectedPhotons"));
	
	if(photons->size() == 0) return StatusCode::SUCCESS;
	
	std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer*> singlephotons = xAOD::shallowCopyContainer(*photons);
	xAOD::PhotonContainer::iterator photon  = singlephotons.first->begin();
	//xAOD::PhotonContainer::iterator phLast  = singlephotons.first->end();
	
	//for(; photon != photon; ++photon){
		
		m_photonSelector->decorateWithInfo((*photon));
		fillPhotonVariables((*photon));
		
		if(m_saveCells) {
		
			CHECK(m_egammaCellDecorator->decorate((*photon)));
			fillClusterCells((*photon), "photon");
		}	

		m_tree->Fill();	
	
	//}


	return StatusCode::SUCCESS;
}


void NTupleMaker::fillClusterCells(const xAOD::Egamma* egamma, TString object) {


	for(auto lr : m_N_lr) {
		
		for(unsigned int i = 0; i<m_N_eta.size(); i++){
			
			int eta = m_N_eta[i];
			int phi = m_N_phi[i];
			
			if(lr != 4) {
			
				m_cells_e[Form("%s.%ix%iClusterLr%iE",         object.Data(), eta, phi, lr)].clear();
				m_cells_eta[Form("%s.%ix%iClusterLr%iEta",     object.Data(), eta, phi, lr)].clear();
				m_cells_phi[Form("%s.%ix%iClusterLr%iPhi",     object.Data(), eta, phi, lr)].clear();
				m_cells_incluster[Form("%s.%ix%iClusterLr%iInCluster", object.Data(), eta, phi, lr)].clear();
				m_cluster_size[Form("%s.%ix%iClusterLr%iSize", object.Data(), eta, phi, lr)] = 0;
				
			}else{
			
				m_cells_e[Form("%s.%ix%iClusterLr%iE",         object.Data(), eta, phi, lr)].clear();
				m_cells_eta[Form("%s.%ix%iClusterLr%iEta",     object.Data(), eta, phi, lr)].clear();
				m_cells_phi[Form("%s.%ix%iClusterLr%iPhi",     object.Data(), eta, phi, lr)].clear();
				m_cells_index[Form("%s.%ix%iClusterLr%iIndex", object.Data(), eta, phi, lr)].clear();
			
			}	
			
			
			if(lr == 4) {
			
				m_cells_e[Form("%s.%ix%iClusterLr%iE",         object.Data(), eta, phi, lr)]   = egamma->auxdecor<std::vector<double>>(Form("%ix%iClusterLr%iE",   eta, phi, lr));
			
			m_cells_eta[Form("%s.%ix%iClusterLr%iEta",     object.Data(), eta, phi, lr)]   = egamma->auxdecor<std::vector<double>>(Form("%ix%iClusterLr%iEta", eta, phi, lr));
			
			m_cells_phi[Form("%s.%ix%iClusterLr%iPhi",     object.Data(), eta, phi, lr)]   = egamma->auxdecor<std::vector<double>>(Form("%ix%iClusterLr%iPhi", eta, phi, lr));
		
			m_cells_index[Form("%s.%ix%iClusterLr%iIndex", object.Data(), eta, phi, lr)] = egamma->auxdecor<std::vector<int>>(Form("%ix%iClusterLr%iIndex",                eta, phi, lr));
			
			
			}else{
			
			
			
			m_cells_e[Form("%s.%ix%iClusterLr%iE",         object.Data(), eta, phi, lr)]   = egamma->auxdecor<std::vector<double>>(Form("%ix%iClusterLr%iE",   eta, phi, lr));
			
			m_cells_eta[Form("%s.%ix%iClusterLr%iEta",     object.Data(), eta, phi, lr)]   = egamma->auxdecor<std::vector<double>>(Form("%ix%iClusterLr%iEta", eta, phi, lr));
			
			m_cells_phi[Form("%s.%ix%iClusterLr%iPhi",     object.Data(), eta, phi, lr)]   = egamma->auxdecor<std::vector<double>>(Form("%ix%iClusterLr%iPhi", eta, phi, lr));
			
			m_cluster_size[Form("%s.%ix%iClusterLr%iSize", object.Data(), eta, phi, lr)]   = egamma->auxdecor<int>(Form("%ix%iClusterLr%iSize",                eta, phi, lr));
			
			m_cells_incluster[Form("%s.%ix%iClusterLr%iInCluster", object.Data(), eta, phi, lr)] = egamma->auxdecor<std::vector<bool>>(Form("%ix%iClusterLr%iInCluster",                eta, phi, lr));
			
			
			}
		}
	}
}


void NTupleMaker::fillShowerShapes(const xAOD::Egamma* egamma, TString object) {

	
	for(auto sw  : m_showers) {
	for(auto flag : m_showerfalg) {
		m_showershapes[Form("%s.%s%s", object.Data(), flag.data(), sw.data())] = egamma->auxdata<float>(Form("%s%s", flag.data(), sw.data()));	
	}}	

}

void NTupleMaker::fillIsolationVars(const xAOD::Egamma* egamma, TString object) {

	for(auto iso : m_isovars) {
	
		m_isolationvar[Form("%s.iso_%s", object.Data(), iso.data()) ] = egamma->auxdata<float>(Form("iso_%s", iso.data()));
	}


}

void NTupleMaker::fillPhotonVariables(const xAOD::Photon* egamma) {

	
	photon_pt  = egamma->pt();
	photon_eta = egamma->eta();
	photon_phi = egamma->phi();
	photon_e   = egamma->e();
	
	photon_cluster_e   = egamma->caloCluster()->e();
	photon_cluster_pt  = egamma->caloCluster()->pt();
	photon_cluster_eta = egamma->caloCluster()->eta();
	photon_cluster_phi = egamma->caloCluster()->phi();
	photon_cluster_eta1= egamma->caloCluster()->etaBE(1);
	photon_cluster_eta2= egamma->caloCluster()->etaBE(2);
	
	photon_convFlag = egamma->conversionType();
	photon_convR    = egamma->conversionRadius();
	
	photon_looseOff    = egamma->auxdata<bool>("looseOff");
	photon_looseOn     = egamma->auxdata<bool>("looseOn"); 
	photon_tightOffInc = egamma->auxdata<bool>("tightOffInc");
	photon_tightOffDep = egamma->auxdata<bool>("tightOffDep");
	photon_tightOn     = egamma->auxdata<bool>("tightOn");
	
	photon_isoloose    = egamma->auxdata<bool>("isoloose");
	photon_isotight    = egamma->auxdata<bool>("isotight");
		
	fillShowerShapes(egamma, "photon");
	fillIsolationVars(egamma, "photon");
	
	photon_istruthmatch      = egamma->auxdata<bool>("istruthmatch");
	photon_istruthconversion = egamma->auxdata<bool>("istruthconversion");
	photon_pdg               = egamma->auxdata<int>("pdg");
	photon_truth_origin      = egamma->auxdata<int>("truth_origin");
	photon_truth_type        = egamma->auxdata<int>("truth_type");
	photon_mother_pdg        = egamma->auxdata<int>("MotherPDG");

}

