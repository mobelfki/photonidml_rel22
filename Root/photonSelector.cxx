


#include <NTupleMaker/photonSelector.h>



photonSelector :: photonSelector (const std::string& name) : AsgTool (name) {


	declareProperty("photonCalibrationAndSmearingTool", m_photonCalibrationAndSmearingTool, "the EgammaCalibrationAndSmearing tool");
	declareProperty("photonIsolationCorrectionTool",    m_photonIsolationCorrectionTool, "the IsolationCorrection tool");
	declareProperty("photonAmbiguityTool",              m_photonAmbiguityTool, "the ambiguity tool");
	declareProperty("photonShowerShape",                m_fudgeMCTool, "the fudge factor tool");

	declareProperty("inputContainer",  m_inputContainer  = "Photons");
	declareProperty("outputContainer", m_outputContainer = "selectedPhotons");

	declareProperty("minPt", m_minPt = 10.);
	declareProperty("minEta", m_minEta = 2.5);
	
	declareProperty("CfgLooseOffline", m_cfg_offline_loose);

	declareProperty("CfgTightIncOffline", m_cfg_offline_tightinc);
	declareProperty("CfgTightDepOffline", m_cfg_offline_tightdep);
	
	declareProperty("CfgLooseOnline", m_cfg_online_loose);
	declareProperty("CfgTightOnline", m_cfg_online_tight);
	
	declareProperty("IsoLooseWP", m_isolooseWP);
	declareProperty("IsoTightWP", m_isotightWP);
	
	declareProperty("isMC", m_isMC);					

}


StatusCode photonSelector::initialize() {

	ATH_MSG_INFO ("Initializing " << name() << "...");
	
	CHECK(m_photonCalibrationAndSmearingTool.retrieve() );
  	CHECK(m_photonIsolationCorrectionTool.retrieve() );
  	CHECK(m_photonAmbiguityTool.retrieve());
	CHECK(m_fudgeMCTool.retrieve());
  	
  	m_photonSelectionTool_loose     = new AsgPhotonIsEMSelector("offline_loose");
	m_photonSelectionTool_looseisEM = new AsgPhotonIsEMSelector("online_loose");

	
	m_photonSelectionTool_tightinc     = new AsgPhotonIsEMSelector("offline_tight_inc");
	m_photonSelectionTool_tightdep     = new AsgPhotonIsEMSelector("offline_tight_dep");
	m_photonSelectionTool_tightisEM = new AsgPhotonIsEMSelector("online_tight");	

	
	CHECK(m_photonSelectionTool_loose->setProperty("isEMMask", egammaPID::PhotonLoose));
	CHECK(m_photonSelectionTool_looseisEM->setProperty("isEMMask", egammaPID::PhotonLoose));

	
	CHECK(m_photonSelectionTool_tightinc->setProperty("isEMMask", egammaPID::PhotonTight));
	CHECK(m_photonSelectionTool_tightdep->setProperty("isEMMask", egammaPID::PhotonTight));
	CHECK(m_photonSelectionTool_tightisEM->setProperty("isEMMask", egammaPID::PhotonTight));

	CHECK(m_photonSelectionTool_loose->setProperty("ConfigFile", m_cfg_offline_loose) );
	CHECK(m_photonSelectionTool_looseisEM->setProperty("ConfigFile", m_cfg_online_loose) );

	
	CHECK(m_photonSelectionTool_tightinc->setProperty("ConfigFile", m_cfg_offline_tightinc) );
	CHECK(m_photonSelectionTool_tightdep->setProperty("ConfigFile", m_cfg_offline_tightdep) );
	CHECK(m_photonSelectionTool_tightisEM->setProperty("ConfigFile", m_cfg_online_tight) );

	
	CHECK(m_photonSelectionTool_loose->initialize());							
	CHECK(m_photonSelectionTool_looseisEM->initialize());							

							
	CHECK(m_photonSelectionTool_tightinc->initialize());							
	CHECK(m_photonSelectionTool_tightdep->initialize());							
	CHECK(m_photonSelectionTool_tightisEM->initialize());							

	
	m_photonIsolationTool_loose = new CP::IsolationSelectionTool("isolation_loose");
	m_photonIsolationTool_tight = new CP::IsolationSelectionTool("isolation_tight");
	
	CHECK(m_photonIsolationTool_loose->setProperty("PhotonWP", m_isolooseWP));
	CHECK(m_photonIsolationTool_tight->setProperty("PhotonWP", m_isotightWP));	
	
	CHECK(m_photonIsolationTool_loose->initialize());							
	CHECK(m_photonIsolationTool_tight->initialize());													
			
	return StatusCode::SUCCESS;
}

StatusCode photonSelector::finalize() {

       ATH_MSG_INFO ("Finalizing " << name() << "...");

       return StatusCode::SUCCESS;
    
}

StatusCode photonSelector::execute(const xAOD::EventInfo *eventInfo) {

	ATH_MSG_INFO ("Executing " << name() << "...");
	
	
	const xAOD::PhotonContainer* photons = nullptr;
  	CHECK (evtStore()->retrieve (photons, m_inputContainer));
  	
  	std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer*> photonsCopy = xAOD::shallowCopyContainer(*photons);
	xAOD::PhotonContainer::iterator photon  = photonsCopy.first->begin();
	xAOD::PhotonContainer::iterator phLast  = photonsCopy.first->end();
	
	
	int i = 0;
	for(; photon != phLast; ++photon)
	{
	
		if(!(*photon)->isGoodOQ(xAOD::EgammaParameters::BADCLUSPHOTON)) continue;
		
		if((*photon)->author() == xAOD::EgammaParameters::AuthorCaloTopo35) continue; // don't keep topo seeded photons
	
		m_photonCalibrationAndSmearingTool->setRandomSeed(eventInfo->eventNumber() + 100 * i++);
		if( m_photonCalibrationAndSmearingTool->applyCorrection(**photon) != CP::CorrectionCode::Ok) ATH_MSG_DEBUG("cannot calibrate the photon");
	
		if( m_photonIsolationCorrectionTool->applyCorrection(**photon) != CP::CorrectionCode::Ok) ATH_MSG_DEBUG("cannot correct the photon isolation");
		
		if ( not (bool)m_photonAmbiguityTool->accept(**photon)) continue;
		
		const xAOD::CaloCluster *cluster = (*photon)->caloCluster();
		
		float eta      = cluster->etaBE(2);
		float cosheta  = cosh(eta);
		float energy   = cluster->e();
		float eT       = (cosheta != 0.) ? energy/cosheta : 0.;
		
		if( fabs(eta) > m_minEta ) continue;
		if( fabs(eta) > 1.37 && fabs(eta) < 1.52 ) continue;
		if( eT < m_minPt) continue;
		
		if( !(!( ((*photon)->OQ()&1073741824)!=0 || ( ((*photon)->OQ()&134217728)!=0 && ((*photon)->showerShapeValue(xAOD::EgammaParameters::Reta) > 0.98 || (*photon)->showerShapeValue(xAOD::EgammaParameters::f1) > 0.4 || ((*photon)->OQ()&67108864)!=0)) )) ) continue;
		
	}
	
	
	CHECK(evtStore()->record( photonsCopy.first,    m_outputContainer));
  	CHECK(evtStore()->record( photonsCopy.second,   m_outputContainer+"Aux"));
   	
	return StatusCode::SUCCESS;
}


void photonSelector :: decorateWithTruthInfo(xAOD::Photon* egamma, bool isMC)
{
	const xAOD::TruthParticle *truthPh = xAOD::TruthHelpers::getTruthParticle(*egamma);

	bool isTruthMatch = false;
	bool TruthConversion = false;
	int  PDG = -99;

	if(truthPh != nullptr && isMC)
	{

		bool isStable = truthPh->status() == 1 && truthPh->barcode() < 200000;
		bool isPhoton = MC::isPhoton(truthPh->pdgId());
		bool isNotFromHadron = notFromHadron(truthPh);

		isTruthMatch = isStable && isPhoton && isNotFromHadron;

		TruthConversion = xAOD::EgammaHelpers::isTrueConvertedPhoton(truthPh);

		PDG = truthPh->pdgId();

	}
	
	egamma->auxdata<bool>("istruthmatch")      = isTruthMatch;
	egamma->auxdata<bool>("istruthconversion") = TruthConversion;
	egamma->auxdata<int>("pdg")                = PDG;
	egamma->auxdata<int>("truth_origin")       = egamma->auxdata<int>("truthOrigin");
	egamma->auxdata<int>("truth_type")         = egamma->auxdata<int>("truthType");
	egamma->auxdata<int>("MotherPDG")          = truthPh!= nullptr && truthPh->nParents() > 0 ? truthPh->parent(0)->pdgId() : -99;
	return;
}

bool photonSelector :: notFromHadron(const xAOD::TruthParticle *ptcl) {

    int ID = ptcl->pdgId();

    // if the particle is a hadron, return false
    if (MC::isHadron(ID)) { return false; }

    // if there are no parents, not from hadron
    if (ptcl->nParents() == 0) { return true; }

    const xAOD::TruthParticle *parent = ptcl->parent(0);
    int parentID = parent->pdgId();

    if (MC::isHadron(parentID)) { return false; } // from hadron!

    if (MC::isTau(parentID) || parentID == ID) { return notFromHadron(parent); }

    // if we get here, all is good
    return true;
}

void photonSelector :: decorateWithInfo(xAOD::Photon* photon) {


	if(m_isMC) {
		
			photon->showerShapeValue(photon->auxdata<float>("unfudged_rhad1"),  xAOD::EgammaParameters::Rhad1);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_rhad"),   xAOD::EgammaParameters::Rhad);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_e277"),   xAOD::EgammaParameters::e277);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_reta"),   xAOD::EgammaParameters::Reta);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_rphi"),   xAOD::EgammaParameters::Rphi);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_weta2"),  xAOD::EgammaParameters::weta2);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_f1"),     xAOD::EgammaParameters::f1);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_wtots1"), xAOD::EgammaParameters::wtots1);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_weta1"),  xAOD::EgammaParameters::weta1);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_fracs1"), xAOD::EgammaParameters::fracs1);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_eratio"), xAOD::EgammaParameters::Eratio);
			photon->showerShapeValue(photon->auxdata<float>("unfudged_deltae"), xAOD::EgammaParameters::DeltaE);

			if(m_fudgeMCTool->applyCorrection(*photon)  != CP::CorrectionCode::Ok) ATH_MSG_DEBUG("cannot correct the photon shower shape");
			
			photon->showerShapeValue(photon->auxdata<float>("rhad1"),  xAOD::EgammaParameters::Rhad1);
			photon->showerShapeValue(photon->auxdata<float>("rhad"),   xAOD::EgammaParameters::Rhad);
			photon->showerShapeValue(photon->auxdata<float>("e277"),   xAOD::EgammaParameters::e277);
			photon->showerShapeValue(photon->auxdata<float>("reta"),   xAOD::EgammaParameters::Reta);
			photon->showerShapeValue(photon->auxdata<float>("rphi"),   xAOD::EgammaParameters::Rphi);
			photon->showerShapeValue(photon->auxdata<float>("weta2"),  xAOD::EgammaParameters::weta2);
			photon->showerShapeValue(photon->auxdata<float>("f1"),     xAOD::EgammaParameters::f1);
			photon->showerShapeValue(photon->auxdata<float>("wtots1"), xAOD::EgammaParameters::wtots1);
			photon->showerShapeValue(photon->auxdata<float>("weta1"),  xAOD::EgammaParameters::weta1);
			photon->showerShapeValue(photon->auxdata<float>("fracs1"), xAOD::EgammaParameters::fracs1);
			photon->showerShapeValue(photon->auxdata<float>("eratio"), xAOD::EgammaParameters::Eratio);
			photon->showerShapeValue(photon->auxdata<float>("deltae"), xAOD::EgammaParameters::DeltaE);
		}	
			
		// decorate photons with identification	
		
		//static SG::AuxElement::Decorator< bool > looseOff( "looseOff" );
		///looseOff(*(*photon)) = (bool)m_photonSelectionTool_loose->accept(*photon);
		
		photon->auxdata<bool>("looseOff")    = (bool)m_photonSelectionTool_loose->accept(photon);
		photon->auxdata<bool>("looseOn")     = (bool)m_photonSelectionTool_looseisEM->accept(photon);
		photon->auxdata<bool>("tightOffInc") = (bool)m_photonSelectionTool_tightinc->accept(photon);
		photon->auxdata<bool>("tightOffDep") = (bool)m_photonSelectionTool_tightdep->accept(photon);
		photon->auxdata<bool>("tightOn")     = (bool)m_photonSelectionTool_tightisEM->accept(photon);
		
		photon->auxdata<bool>("isoloose")    = (bool)m_photonIsolationTool_loose->accept(*photon);
		photon->auxdata<bool>("isotight")    = (bool)m_photonIsolationTool_tight->accept(*photon);
		
		
		photon->auxdata<int>("convFlag")     = xAOD::EgammaHelpers::conversionType(photon);
		photon->auxdata<float>("convR")      = xAOD::EgammaHelpers::conversionRadius(photon);
		
		
		float m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::ptcone20);
		photon->auxdata<float>("iso_ptcone20") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::ptcone30);
		photon->auxdata<float>("iso_ptcone30") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::ptcone40);
		photon->auxdata<float>("iso_ptcone40") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::etcone20);
		photon->auxdata<float>("iso_etcone20") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::etcone30);
		photon->auxdata<float>("iso_etcone30") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::etcone40);
		photon->auxdata<float>("iso_etcone40") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::topoetcone20);
		photon->auxdata<float>("iso_topoetcone20") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::topoetcone30);
		photon->auxdata<float>("iso_topoetcone30") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::topoetcone40);
		photon->auxdata<float>("iso_topoetcone40") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::ptvarcone20);
		photon->auxdata<float>("iso_ptvarcone20") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::ptvarcone30);
		photon->auxdata<float>("iso_ptvarcone30") = m_iso;
		
		photon->isolationValue(m_iso, xAOD::Iso::ptvarcone40);
		photon->auxdata<float>("iso_ptvarcone40") = m_iso;

		
		decorateWithTruthInfo(photon, m_isMC);
}



