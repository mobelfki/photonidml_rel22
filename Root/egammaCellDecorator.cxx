

#include <NTupleMaker/egammaCellDecorator.h>


egammaCellDecorator :: egammaCellDecorator (const std::string& name) : AsgTool (name) {

	declareProperty("CaloCellContainer", m_CaloCellContainer = "AODCellContainer");
	declareProperty("CaloFillRectangularCluster_7x11", m_caloFillRectangularTool_7x11);
	declareProperty("CaloFillRectangularCluster_5x5",  m_caloFillRectangularTool_5x5);
	declareProperty("CaloFillRectangularCluster_3x5",  m_caloFillRectangularTool_3x5);		
	declareProperty("etaSize", m_etaSize);
	declareProperty("phiSize", m_phiSize);
	
}


StatusCode egammaCellDecorator::initialize() {

	ATH_MSG_INFO ("Initializing " << name() << "...");
	
	if(m_etaSize.size() != m_phiSize.size()) {
		ATH_MSG_ERROR("eta, phi clusters size are not correctly set");
	}
	
	CHECK(m_caloMgrKey.initialize());
	
	CHECK(m_caloFillRectangularTool_7x11.retrieve());
	CHECK(m_caloFillRectangularTool_5x5.retrieve());
	CHECK(m_caloFillRectangularTool_3x5.retrieve());
	
	m_CaloTools[ Form("CaloFillRectangularCluster_%ix%i", 7, 11)] = dynamic_cast<CaloFillRectangularCluster* >( &(*m_caloFillRectangularTool_7x11 ));
	m_CaloTools[ Form("CaloFillRectangularCluster_%ix%i", 5, 5)]  = dynamic_cast<CaloFillRectangularCluster* >( &(*m_caloFillRectangularTool_5x5 ));
	m_CaloTools[ Form("CaloFillRectangularCluster_%ix%i", 3, 5)]  = dynamic_cast<CaloFillRectangularCluster* >( &(*m_caloFillRectangularTool_3x5 ));		
	return StatusCode::SUCCESS;
}

StatusCode egammaCellDecorator::finalize() {

       ATH_MSG_INFO ("Finalizing " << name() << "...");

       return StatusCode::SUCCESS;
    
}

StatusCode egammaCellDecorator::decorate(const xAOD::Egamma* egamma) {

	ATH_MSG_INFO( "Start decoration ... ");	
	
	for (unsigned int i = 0; i<m_etaSize.size(); i++) {
	
		SG::ReadCondHandle<CaloDetDescrManager> caloMgr{m_caloMgrKey};
    		CHECK(caloMgr.isValid());
    		
		if (not egamma or not egamma->caloCluster()) return StatusCode::SUCCESS;
		
		const CaloCellContainer* cellCont(0);	  	
		int m_eta_size = m_etaSize[i];
		int m_phi_size = m_phiSize[i];
			
		if (evtStore()->retrieve(cellCont, m_CaloCellContainer).isFailure()) {
				ATH_MSG_WARNING(m_CaloCellContainer << " not found");
		}	
			
		
		std::unique_ptr<xAOD::CaloCluster> egcClone = CaloClusterStoreHelper::makeCluster( cellCont,
				                            egamma->caloCluster()->eta0(),
				                            egamma->caloCluster()->phi0(),
				                            egamma->caloCluster()->clusterSize());
				                            
	    	m_CaloTools[ Form("CaloFillRectangularCluster_%ix%i", m_eta_size, m_phi_size)]->makeCorrection(Gaudi::Hive::currentContext(), egcClone.get());
	    	 	
		
		const CaloClusterCellLink* cellLinks = egamma->caloCluster()->getCellLinks();
		
		CaloClusterCellLink::const_iterator it_cell = cellLinks->begin();
    		CaloClusterCellLink::const_iterator it_cell_e = cellLinks->end();
    		
    		std::vector<std::size_t> cl_id_L1;
    		std::vector<std::size_t> cl_id_L2;
    		std::vector<std::size_t> cl_id_L3;
    		std::vector<std::size_t> cl_id_PreS;

    		for(;it_cell!=it_cell_e; ++it_cell) {
        		const CaloCell* cell = (*it_cell);
        		if (!cell) continue;
			
        		int sampling = cell->caloDDE()->getSampling();
        		
        		if (sampling == CaloCell_ID::EMB1 || sampling == CaloCell_ID::EME1) {
            			cl_id_L1.push_back(cell->ID().get_identifier32().get_compact());
            			
        		}
        		else if (sampling == CaloCell_ID::EMB2 || sampling == CaloCell_ID::EME2) {
            			cl_id_L2.push_back(cell->ID().get_identifier32().get_compact());
        		}
        		else if (sampling == CaloCell_ID::EMB3 || sampling == CaloCell_ID::EME3) {
            			cl_id_L3.push_back(cell->ID().get_identifier32().get_compact());
        		}
        		else if (sampling == CaloCell_ID::PreSamplerB || sampling == CaloCell_ID::PreSamplerE ){
        			cl_id_PreS.push_back(cell->ID().get_identifier32().get_compact());
        		} 
        		
    		}
		
		auto first_cell = egcClone->cell_begin();
		auto last_cell  = egcClone->cell_end();
		
		int ncelll1 = 0;
		int ncelll2 = 0;
		int ncelll3 = 0;
		int npres = 0;
		int ncell = 0; 
		  
		std::vector<double> EClusterLr1Eta, EClusterLr1Phi, EClusterLr1E;
		std::vector<bool> EClusterLr1InCluster;
		std::vector<double> EClusterLr2Eta, EClusterLr2Phi, EClusterLr2E;
		std::vector<bool> EClusterLr2InCluster;
		std::vector<double> EClusterLr3Eta, EClusterLr3Phi, EClusterLr3E;
		std::vector<bool> EClusterLr3InCluster;
		std::vector<double> EClusterPreSEta, EClusterPreSPhi, EClusterPreSE;
		std::vector<bool> EClusterPreSInCluster;
		std::vector<double> EClusterLrRestEta, EClusterLrRestPhi, EClusterLrRestE;
		std::vector<int> EClusterLrRestIndex;

		for (;first_cell != last_cell; ++first_cell,++ncell) {
		
			const CaloCell* tcell = *first_cell;
			int sampling = tcell->caloDDE()->getSampling();

			if( sampling == CaloCell_ID::EMB1 || sampling == CaloCell_ID::EME1 ){
			
				EClusterLr1Eta.push_back(tcell->eta());
				EClusterLr1Phi.push_back(tcell->phi());
				EClusterLr1E.push_back(tcell->e());
				
				EClusterLr1InCluster.push_back(std::count(cl_id_L1.begin(), cl_id_L1.end(), tcell->ID().get_identifier32().get_compact()));
					
				ncelll1++;
			}

			if( sampling == CaloCell_ID::EMB2 || sampling== CaloCell_ID::EME2 ){
				EClusterLr2Eta.push_back(tcell->eta());
				EClusterLr2Phi.push_back(tcell->phi());
				EClusterLr2E.push_back(tcell->e());
				
				EClusterLr2InCluster.push_back(std::count(cl_id_L2.begin(), cl_id_L2.end(), tcell->ID().get_identifier32().get_compact()));

				ncelll2++;
			}

			if( sampling == CaloCell_ID::EMB3 || sampling== CaloCell_ID::EME3 ){
				EClusterLr3Eta.push_back(tcell->eta());
				EClusterLr3Phi.push_back(tcell->phi());
				EClusterLr3E.push_back(tcell->e());
				
				EClusterLr3InCluster.push_back(std::count(cl_id_L3.begin(), cl_id_L3.end(), tcell->ID().get_identifier32().get_compact()));

				ncelll3++;
			}

			if( sampling == CaloCell_ID::PreSamplerB || sampling == CaloCell_ID::PreSamplerE ){
				EClusterPreSEta.push_back(tcell->eta());
				EClusterPreSPhi.push_back(tcell->phi());
				EClusterPreSE.push_back(tcell->e());
				
				EClusterPreSInCluster.push_back(std::count(cl_id_PreS.begin(), cl_id_PreS.end(), tcell->ID().get_identifier32().get_compact()));

				npres++;
			}
			
			if( sampling > 7) {
				EClusterLrRestEta.push_back(tcell->eta());
				EClusterLrRestPhi.push_back(tcell->phi());
				EClusterLrRestE.push_back(tcell->e());
				EClusterLrRestIndex.push_back(sampling);
			}
		
		}
		
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr1Eta",m_eta_size,m_phi_size)) = EClusterLr1Eta;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr1Phi",m_eta_size,m_phi_size)) = EClusterLr1Phi;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr1E",m_eta_size,m_phi_size))   = EClusterLr1E;

		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr2Eta",m_eta_size,m_phi_size)) = EClusterLr2Eta;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr2Phi",m_eta_size,m_phi_size)) = EClusterLr2Phi;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr2E",m_eta_size,m_phi_size))   = EClusterLr2E;

		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr3Eta",m_eta_size,m_phi_size)) = EClusterLr3Eta;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr3Phi",m_eta_size,m_phi_size)) = EClusterLr3Phi;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr3E",m_eta_size,m_phi_size))   = EClusterLr3E;
		
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr0Eta",m_eta_size,m_phi_size)) = EClusterPreSEta;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr0Phi",m_eta_size,m_phi_size)) = EClusterPreSPhi;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr0E",m_eta_size,m_phi_size))   = EClusterPreSE;

		egamma->auxdecor<int>(Form("%dx%dClusterLr1Size",m_eta_size, m_phi_size))  = ncelll1;
		egamma->auxdecor<int>(Form("%dx%dClusterLr2Size",m_eta_size, m_phi_size))  = ncelll2;
		egamma->auxdecor<int>(Form("%dx%dClusterLr3Size",m_eta_size, m_phi_size))  = ncelll3;
		egamma->auxdecor<int>(Form("%dx%dClusterLr0Size",m_eta_size, m_phi_size))  = npres;
		
		egamma->auxdecor<std::vector<bool>>(Form("%dx%dClusterLr1InCluster",m_eta_size, m_phi_size))  = EClusterLr1InCluster;
		egamma->auxdecor<std::vector<bool>>(Form("%dx%dClusterLr2InCluster",m_eta_size, m_phi_size))  = EClusterLr2InCluster;
		egamma->auxdecor<std::vector<bool>>(Form("%dx%dClusterLr3InCluster",m_eta_size, m_phi_size))  = EClusterLr3InCluster;
		egamma->auxdecor<std::vector<bool>>(Form("%dx%dClusterLr0InCluster",m_eta_size, m_phi_size))  = EClusterPreSInCluster;
		
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr4Eta",m_eta_size,m_phi_size)) = EClusterLrRestEta;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr4Phi",m_eta_size,m_phi_size)) = EClusterLrRestPhi;
		egamma->auxdecor<std::vector<double>>(Form("%dx%dClusterLr4E",m_eta_size,m_phi_size))   = EClusterLrRestE;
		egamma->auxdecor<std::vector<int>>(Form("%dx%dClusterLr4Index",m_eta_size,m_phi_size))   = EClusterLrRestIndex;
	}
	

	return StatusCode::SUCCESS;
}

