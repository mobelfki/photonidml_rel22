


#include <NTupleMaker/muonSelector.h>



muonSelector :: muonSelector (const std::string& name) : AsgTool (name) {


	declareProperty("muonCalibrationTool", m_muonCalibrationTool, "the calibration tool");
	declareProperty("muonIsolationTool",   m_muonIsolationTool, "the IsolationSelection tool");
	declareProperty("muonSelectionTool",   m_muonSelectionTool, "the Selection tool");
	
	declareProperty("inputContainer",  m_inputContainer  = "Muons");
	declareProperty("outputContainer", m_outputContainer = "selectedMuons");
	declareProperty("vertexContainer", m_vertexContainer = "PrimaryVertices"); 

	declareProperty("minPt", m_minPt = 10.);
	declareProperty("minEta", m_minEta = 2.5);
	declareProperty("minZ0", m_minZ0 = 10.);
	declareProperty("mind0", m_minD0 = 10.);
}


StatusCode muonSelector::initialize() {

	ATH_MSG_INFO ("Initializing " << name() << "...");
	
	CHECK(m_muonCalibrationTool.retrieve());
	CHECK(m_muonIsolationTool.retrieve());
	CHECK(m_muonSelectionTool.retrieve());

	return StatusCode::SUCCESS;
}

StatusCode muonSelector::finalize() {

       ATH_MSG_INFO ("Finalizing " << name() << "...");

       return StatusCode::SUCCESS;
    
}

StatusCode muonSelector::execute() {

	ATH_MSG_INFO ("Executing " << name() << "...");
	
	const xAOD::MuonContainer* muons = nullptr;
	CHECK( evtStore()->retrieve(muons, m_inputContainer));
	
	const xAOD::VertexContainer *vtxContainer = nullptr;
	CHECK(evtStore()->retrieve(vtxContainer, m_vertexContainer));
	
	const xAOD::Vertex* pvtx = nullptr;
	if(vtxContainer->size() > 0) pvtx = (*vtxContainer)[0];
	
	double m_pvZ = pvtx->z();
	
	std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer*> mounsCopy = xAOD::shallowCopyContainer(*muons);
	xAOD::MuonContainer::iterator muon= mounsCopy.first->begin();
	xAOD::MuonContainer::iterator muLast  = mounsCopy.first->end();
	
	for(; muon != muLast; ++muon)
	{
	
		if( m_muonCalibrationTool->applyCorrection(**muon) != CP::CorrectionCode::Ok) ATH_MSG_DEBUG("cannot calibrate the muon");
		
		if(!m_muonSelectionTool->accept((*muon))) continue;
		if(!m_muonIsolationTool->accept(*(*muon))) continue;
		 
		if((*muon)->pt() * 1e-3 < m_minPt ) continue;
		if(fabs((*muon)->eta()) > m_minEta ) continue;
		if((*muon)->muonType() == xAOD::Muon::Combined){
		
			const ElementLink< xAOD::TrackParticleContainer >& link = (*muon)->primaryTrackParticleLink();
		
			double d0   = 100.;
			double d0err = 100.; 
			double z0   = 100.;
		
			if( link.isValid() ) {
				const xAOD::TrackParticle* track = *link;
				d0    = fabs(track->d0());
				d0err = sqrt(track->definingParametersCovMatrix () (0,0));
				z0    = fabs(track->z0() + track->vz() - m_pvZ) * sin(track->theta());		
			}
		
			if( z0 > m_minZ0) continue; 
			if( d0/d0err > m_minD0 ) continue; 
		}
	
	}
	
	CHECK(evtStore()->record( mounsCopy.first,    m_outputContainer));
  	CHECK(evtStore()->record( mounsCopy.second,   m_outputContainer+"Aux"));
   	
	return StatusCode::SUCCESS;
}
