
#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

from PyUtils import AthFile

import os
import sys

#files=['/eos/user/m/mobelfki/AOD/mumug/mc20_13TeV/AOD.27390676._000001.pool.root.1']

#files=['/eos/user/m/mobelfki/AOD/photonid/mc23_13p6TeV.700771.Sh_2214_mumugamma.recon.AOD.e8514_s4159_r14799/AOD.34294613._000630.pool.root.1']
#files=['/eos/user/m/mobelfki/AOD/photonid/mc23_13p6TeV.700771.Sh_2214_mumugamma.recon.AOD.e8514_s4159_r15224/AOD.36268526._000430.pool.root.1']
files=['/eos/user/m/mobelfki/AOD/photonid/mc23_13p6TeV.801649.Py8_gammajet_frag_DP8_17_FullSW.merge.AOD.e8514_e8528_s4111_s4114_r14622_r14663/AOD.33395154._000076.pool.root.1']
#files = ['/eos/user/m/mobelfki/AOD/photonid/mc23_valid/AOD.32397323._001901.pool.root.1']

import AthenaPoolCnvSvc.ReadAthenaPool
jps.AthenaCommonFlags.FilesInput = files


jps.AthenaCommonFlags.AccessMode = "POOLAccess" 

#theApp.EvtMax=10

jps.AthenaCommonFlags.HistOutputs = ["OUTPUT:NTupleMaker_mc23_latest.root"]

#af = AthFile.fopen(ServiceMgr.EventSelector.InputCollections[0])
#isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
#run_number = af.run_number[0]


af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
run_number = af.run_number[0]

from AthenaCommon.AlgSequence import AlgSequence
topSequence=AlgSequence()

from AthenaCommon.GlobalFlags import globalflags


from RecExConfig.InputFilePeeker import inputFileSummary
globalflags.DataSource = 'data' if inputFileSummary['evt_type'][0] == "IS_DATA" else 'geant4'
globalflags.DetDescrVersion = inputFileSummary['geometry']


from RecExConfig import AutoConfiguration
AutoConfiguration.ConfigureSimulationOrRealData() #configures DataSource global flag
AutoConfiguration.ConfigureGeo()
AutoConfiguration.ConfigureConditionsTag() #sets globalflags.ConditionsTag
from AthenaCommon.DetFlags import DetFlags
DetFlags.all_setOff() #skip this line out to leave everything on. But this will take longer
DetFlags.detdescr.Calo_setOn() #e.g. if I am accessing CaloCellContainer, I need the calo detector description
include("RecExCond/AllDet_detDescr.py")

from egammaRec import egammaKeys


from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
alg = createAlgorithm ( 'NTupleMaker', 'NTupleMaker' )


####3 Setup the GRL for data 

GRL = [
    'GoodRunsLists/data23_13p6TeV/20230828/data23_13p6TeV.periodAllYear_DetStatus-v110-pro31-06_MERGED_PHYS_StandardGRL_All_Good_25ns.xml',
    'GoodRunsLists/data22_13p6TeV/20230207/data22_13p6TeV.periodAllYear_DetStatus-v109-pro28-04_MERGED_PHYS_StandardGRL_All_Good_25ns.xml',
    'GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml',
    'GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml',
    'GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml',
    'GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml'
]

prw = ['/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PileupReweighting/mc20_common/mc20a.284500.physlite.prw.v1.root',
'/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PileupReweighting/mc20_common/mc20d.300000.physlite.prw.v1.root',
'/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PileupReweighting/mc20_common/mc20e.310000.physlite.prw.v1.root',
'/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PileupReweighting/mc23_common/mc23a.410000.physlite.prw.v2.root',
'/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/PileupReweighting/mc23_common/mc23d.450000.physlite.prw.v1.root'
]

lumical = ['GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root',
     'GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root',
     'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root',
     'GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root', 
     'GoodRunsLists/data22_13p6TeV/20230207/ilumicalc_histograms_None_431810-440613_OflLumi-Run3-003.root',
     'GoodRunsLists/data23_13p6TeV/20230828/ilumicalc_histograms_None_451587-456749_OflLumi-Run3-003.root'
]
#from GoodRunsLists.GoodRunsListsConf import *
#ToolSvc += GoodRunsListSelectorTool() 
#GoodRunsListSelectorTool.GoodRunsListVec = GRL


'''
from GoodRunsListsUser.GoodRunsListsUserConf import *
masterSequence += GRLTriggerSelectorAlg('GRLTriggerAlg1')
masterSequence.GRLTriggerAlg1.GoodRunsListArray = ['minbias_900GeV']
masterSequence.GRLTriggerAlg1.TriggerSelectionRegistration = 'L1_MBTS_1'
'''

# trigger tools

ToolSvc += CfgMgr.Trig__MatchingTool("TrigMatchingTool")
ToolSvc += CfgMgr.Trig__TrigDecisionTool("TrigDecisionTool")

# electron tools

addPrivateTool ( alg, "electronSelector", "electronSelector")
alg.electronSelector.inputContainer  = "Electrons"
alg.electronSelector.outputContainer = "selectedElectrons"
alg.electronSelector.vertexContainer = "PrimaryVertices"
alg.electronSelector.minPt           = 10.0 # GeV
alg.electronSelector.minEta          = 2.47
alg.electronSelector.minZ0           = 10.0 # mm
alg.electronSelector.mind0           = 10.0 # mm 

addPrivateTool ( alg, "electronSelector.electronSelectionTool", "AsgElectronLikelihoodTool")
alg.electronSelector.electronSelectionTool.primaryVertexContainer = 'PrimaryVertices'
alg.electronSelector.electronSelectionTool.ConfigFile = 'ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodMediumOfflineConfig2017_Smooth.conf'

# electron calibration and smearing tool
addPrivateTool( alg, 'electronSelector.electronCalibrationAndSmearingTool', 'CP::EgammaCalibrationAndSmearingTool' )
alg.electronSelector.electronCalibrationAndSmearingTool.ESModel = "es2018_R21_v0"
alg.electronSelector.electronCalibrationAndSmearingTool.decorrelationModel = "FULL_v1"
alg.electronSelector.electronCalibrationAndSmearingTool.randomRunNumber = 123456

# electron Isolation tool
addPrivateTool( alg, 'electronSelector.electronIsolationTool', 'CP::IsolationSelectionTool' )
alg.electronSelector.electronIsolationTool.ElectronWP = 'Loose_VarRad'  

addPrivateTool( alg, 'electronSelector.electronIsolationCorrectionTool', 'CP::IsolationCorrectionTool' )
alg.electronSelector.electronIsolationCorrectionTool.IsMC = isMC


# muons tools 
'''
addPrivateTool ( alg, "muonSelector", "muonSelector")
alg.muonSelector.inputContainer  = "Muons"
alg.muonSelector.outputContainer = "selectedMuons"
alg.muonSelector.vertexContainer = "PrimaryVertices"
alg.muonSelector.minPt           = 10.0 # GeV
alg.muonSelector.minEta          = 2.5
alg.muonSelector.minZ0           = 10.0 # mm
alg.muonSelector.mind0           = 10.0 # mm 

addPrivateTool( alg, 'muonSelector.muonCalibrationTool', 'CP::MuonCalibTool')
alg.muonSelector.muonCalibrationTool.calibMode = 1
#alg.muonSelector.muonCalibrationTool.IsRun3Geo = True

addPrivateTool( alg, 'muonSelector.muonIsolationTool', 'CP::IsolationSelectionTool')
alg.muonSelector.muonIsolationTool.MuonWP = 'Loose_VarRad'

addPrivateTool( alg, 'muonSelector.muonSelectionTool', 'CP::MuonSelectionTool')
alg.muonSelector.muonSelectionTool.MaxEta    = 2.5
#alg.muonSelector.muonSelectionTool.IsRun3Geo = True
alg.muonSelector.muonSelectionTool.MuQuality = 1
'''



#including photon configuration 

# photon tools 

addPrivateTool( alg, "photonSelector", "photonSelector");
alg.photonSelector.inputContainer  = "Photons"
alg.photonSelector.outputContainer = "selectedPhotons"
alg.photonSelector.minPt           = 10.0 # GeV
alg.photonSelector.minEta          = 2.5
alg.photonSelector.isMC            = isMC

alg.photonSelector.CfgLooseOffline    = "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf"
alg.photonSelector.CfgTightIncOffline = "ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf"
alg.photonSelector.CfgTightDepOffline = "NTupleMaker/PhotonIsEMTightSelectorCutDefs_pTdep_mc20_smooth.conf"

alg.photonSelector.CfgLooseOnline = "ElectronPhotonSelectorTools/trigger/rel22_20210611/PhotonIsEMLooseSelectorCutDefs.conf"
alg.photonSelector.CfgTightOnline = "ElectronPhotonSelectorTools/trigger/rel22_20210611/PhotonIsEMTightSelectorCutDefs.conf"

alg.photonSelector.IsoLooseWP = "FixedCutLoose"
alg.photonSelector.IsoTightWP = "FixedCutTight"

addPrivateTool( alg, 'photonSelector.photonCalibrationAndSmearingTool', 'CP::EgammaCalibrationAndSmearingTool' )
alg.photonSelector.photonCalibrationAndSmearingTool.ESModel = "es2018_R21_v0"
alg.photonSelector.photonCalibrationAndSmearingTool.decorrelationModel = "FULL_v1"
#alg.photonSelector.photonCalibrationAndSmearingTool.useFastSim = False
alg.photonSelector.photonCalibrationAndSmearingTool.randomRunNumber = 123456

addPrivateTool( alg, 'photonSelector.photonIsolationCorrectionTool', 'CP::IsolationCorrectionTool' )
alg.photonSelector.photonIsolationCorrectionTool.IsMC = isMC
alg.photonSelector.photonIsolationCorrectionTool.Apply_ddshifts = False
alg.photonSelector.photonIsolationCorrectionTool.Apply_SC_leakcorr = False
alg.photonSelector.photonIsolationCorrectionTool.CorrFile = "IsolationCorrections/v6/isolation_ptcorrections_rel22_mc20.root"
alg.photonSelector.photonIsolationCorrectionTool.ToolVer = "REL21"


addPrivateTool( alg, 'photonSelector.photonAmbiguityTool', 'EGammaAmbiguityTool')

addPrivateTool( alg, 'photonSelector.photonShowerShape', 'ElectronPhotonVariableCorrectionTool')
alg.photonSelector.photonShowerShape.ConfigFile = "EGammaVariableCorrection/TUNE25/ElPhVariableNominalCorrection.conf"

from AthenaConfiguration.ComponentFactory import CompFactory
CaloFillRectangularCluster_7x11 = CompFactory.CaloFillRectangularCluster( name = "CaloFillRectangularCluster_%sx%s" % (7, 11),  eta_size = 7, phi_size = 11, cells_name = egammaKeys.caloCellKey(), fill_cluster = True)	

CaloFillRectangularCluster_5x5 = CompFactory.CaloFillRectangularCluster( name = "CaloFillRectangularCluster_%sx%s" % (5, 5),  eta_size = 5, phi_size = 5, cells_name = egammaKeys.caloCellKey(), fill_cluster = True)	

CaloFillRectangularCluster_3x5 = CompFactory.CaloFillRectangularCluster( name = "CaloFillRectangularCluster_%sx%s" % (3, 5),  eta_size = 3, phi_size = 5, cells_name = egammaKeys.caloCellKey(), fill_cluster = True)	

ToolSvc += CaloFillRectangularCluster_7x11
ToolSvc += CaloFillRectangularCluster_5x5
ToolSvc += CaloFillRectangularCluster_3x5

addPrivateTool( alg, 'egammaCellDecorator', "egammaCellDecorator") 
alg.egammaCellDecorator.CaloCellContainer = egammaKeys.caloCellKey()
alg.egammaCellDecorator.CaloFillRectangularCluster_7x11 = CaloFillRectangularCluster_7x11
alg.egammaCellDecorator.CaloFillRectangularCluster_5x5  = CaloFillRectangularCluster_5x5
alg.egammaCellDecorator.CaloFillRectangularCluster_3x5  = CaloFillRectangularCluster_3x5
alg.egammaCellDecorator.etaSize = [3, 5, 7]
alg.egammaCellDecorator.phiSize = [5, 5, 11]



triggers = ["HLT_e26_lhtight_ivarloose_L1EM22VHI", "HLT_e60_lhmedium_L1EM22VHI", "HLT_e140_lhloose_L1EM22VHI", "HLT_e140_lhloose_noringer_L1EM22VHI", "HLT_2e17_lhvloose_L12EM15VHI",
     "HLT_mu24_ivarmedium_L1MU14FCH",
     "HLT_mu50_L1MU14FCH",
     "HLT_mu60_0eta105_msonly_L1MU14FCH",
     "HLT_mu60_L1MU14FCH",
     "HLT_mu80_msonly_3layersEC_L1MU14FCH",
     "HLT_2mu10_l2mt_L1MU10BOM",
     "HLT_2mu14_L12MU8F",
     "HLT_mu20_ivarmedium_mu8noL1_L1MU14FCH",
     "HLT_mu22_mu8noL1_L1MU14FCH"
]


#topSequence += CfgMgr.NTupleMaker(triggers = triggers, GoodRunsListArray = ['PHYS_StandardGRL_All_Good_25ns']) 

alg.triggers = triggers
alg.GoodRunsListArray = GRL
alg.PRWFiles = prw
alg.isMC = isMC
alg.Algo = "singlePhoton"
alg.saveCells = True
alg.N_eta = alg.egammaCellDecorator.etaSize
alg.N_phi = alg.egammaCellDecorator.phiSize
alg.N_lr  = [0, 1, 2, 3, 4]
alg.saveObjectCells = ['photon']
athAlgSeq += alg

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")


